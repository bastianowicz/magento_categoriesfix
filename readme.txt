tkk_TkkCatalog
bastian charlet <b.charlet@tk-kommunikation.de>
(c) tk-kommunikation

Fix zur Men�f�hrung

Werden Produkte nicht �ber eine Kategorie aufgerufen, ist f�r sie keine Kategorie "aktiv". Somit werden Men�punkte, Breadcrumbs, usw. nicht aktiviert.

Mit diesem Fix wird die erste Kategorie in der das Produkt sich befindet als aktiv gesetzt. Befindet sich das Produkt nur in einer Kategorie, ist das Verhalten eindeutig. Befindet sich das Produkt in mehreren Kategorien wird einfach die erste Kategorie, die aus der Datenbank �bergeben wurde, als aktiv gesetzt.