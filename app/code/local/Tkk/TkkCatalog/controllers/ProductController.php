<?php
/**
 *
 * @category    Tkk
 * @package     Tkk_TkkCatalog
 * @copyright   Copyright (c) tk-kommunikation
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
include(Mage::getBaseDir()."/app/code/core/Mage/Catalog/controllers/ProductController.php");

class Tkk_TkkCatalog_ProductController extends Mage_Catalog_ProductController
{
    

    /**
     * Product view action
     * 
     * @override
     * @see Mage_Catalog_ProductController::viewAction()
     * @author Bastian Charlet <b.charlet@tk-kommunikation.de>
     */
    public function viewAction()
    {
        // Get initial data from request
        $categoryId = (int) $this->getRequest()->getParam('category', false);
        $productId  = (int) $this->getRequest()->getParam('id');
        $specifyOptions = $this->getRequest()->getParam('options');

        /**
         * Set the category as active if none is passed
         * If product belongs to multiple -> use first
         */ 
        if(empty($categoryId)){
	        $cats = Mage::getModel('catalog/product')->load($productId)->getCategoryCollection();
	       	$categoryId = $cats->getFirstItem()->getId();
        }
		
       	
       	// Prepare helper and params
        $viewHelper = Mage::helper('catalog/product_view');

        $params = new Varien_Object();
        $params->setCategoryId($categoryId);
        $params->setSpecifyOptions($specifyOptions);
        

       	// Render page
        try {
            $viewHelper->prepareAndRender($productId, $this, $params);
        } catch (Exception $e) {
            if ($e->getCode() == $viewHelper->ERR_NO_PRODUCT_LOADED) {
                if (isset($_GET['store'])  && !$this->getResponse()->isRedirect()) {
                    $this->_redirect('');
                } elseif (!$this->getResponse()->isRedirect()) {
                    $this->_forward('noRoute');
                }
            } else {
                Mage::logException($e);
                $this->_forward('noRoute');
            }
        }
    }

}
